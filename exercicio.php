Esse exercício é da aula 01 atividade 05.
"Tente criar uma função que recebe um array de números. O retorno da função será a soma de todos os números de dentro do array."

Resultado:

<?php 
	$arrayDeNumeros = array(1,2,3,4,5);
	
	function somaArray($array){
		$numeroDois = 0;
		for ($i=0; $i < sizeof($array) ; $i++) { 
			$numeroUm = $array[$i];
			$numeroDois += $numeroUm;
			}

		return $numeroDois;
	}

	echo somaArray($arrayDeNumeros);
 ?>