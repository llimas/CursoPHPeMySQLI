<html>

<?php 
include 'cabecalho.php';
include("conecta.php");
include("banco-categoria.php");

$categorias = listaCategorias($conexao);
?>

	<html>
    <form action="adiciona-produto.php" method="GET">
        <table class="table">
            <tr>
                <td>Nome</td>
                <td><input type="text" class="form-control" name="nomeProduto" /></td>
            </tr>

            <tr>
                <td>Preço</td>
                <td><input type="number" class="form-control" name="preco" /></td>
            </tr>
            <tr>
                <td>Categoria</td>
                <td>
                    <?php foreach ($categorias as $categoria) : ?>
                        <input type="radio" name="categoria_id" value="<?php echo $categoria['id'] ?>">
                         <?=$categoria['nome']?> </br>
                    <?php endforeach; ?>
                </td>
            </tr>
            <tr>
            	<td>Descrição</td>
            	<td><textarea class="form-control" name="descricao" cols="30" rows="10"></textarea></td>
            </tr>

            <tr>
                <td></td>
                <td><input type="submit" value="Cadastrar" class="btn btn-primary" /></td>
            </tr>

        </table>

    </form>
	</html>

<?php 
include 'rodape.php';
?>

</html>