<?php 
include 'cabecalho.php';
include 'conecta.php';
include 'banco-produto.php';

$nomeProduto = $_GET['nomeProduto'];
$preco = $_GET['preco'];
$descricao = $_GET['descricao'];
$categoriaID = $_GET['categoria_id'];

if(insereProduto($conexao, $nomeProduto, $preco, $descricao, $categoriaID)):
?>

<p class="alert-success">Produto <?php echo $nomeProduto  ?>, <?php echo $preco  ?> adicionado com sucesso.</p>

<?php else: 
$msg = mysqli_error($conexao);
?>

<p class="alert-danger">Produto <?php echo $nomeProduto  ?>, <?php echo $preco  ?> não foi adicionado. Erro: <?php $msg ?></p>

<?php
endif;
include 'rodape.php';
?>