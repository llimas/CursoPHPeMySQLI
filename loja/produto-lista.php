<?php 
require 'cabecalho.php';
require 'conecta.php';
require 'banco-produto.php';

$produtos = listaProdutos($conexao);
?>
<table class="table table-striped table-bordered">
<?php

foreach ($produtos as $produto) :
?>
	<tr>
        <td><?= $produto['nome'] ?></td>
        <td><?= $produto['preco'] ?></td>
         <td><?= $produto['categoria'] ?></td>
            
        <td><?php echo substr($produto['descricao'], 0, 40 )?></td>
        <td>
        	<form action="remove-produto.php" method="post">
            <input type="hidden" name="id" value="<?=$produto['categoria_nome']?>" />
            <button class="btn btn-danger">remover</button>
       		</form>
        </td>
    </tr>

<?php
endforeach;
?>

<?php if (array_key_exists('removido', $_GET)) : ?>
	<p class="alert-success">Produto apagado com sucesso.</p>
<?php
endif

?>
</table>

<?php
require 'rodape.php';
 ?>